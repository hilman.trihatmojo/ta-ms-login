# ta-ms-login

## Overview
The `ta-ms-login` microservice is designed for authentication purposes within the Technical Assessment system. It utilizes Java 17 and Spring Boot 3.2.0 to provide secure login functionality. The primary objective of this microservice is to handle user authentication, and its endpoint path is set to `/api/sso`.

## Prerequisites
Ensure that the MySQL database is installed with the name `technical_assessment`. Additionally, build the project using the following Maven command:
```bash
mvn -U clean install
```

this microservices run in port `8086`

## Usages
After building the project, the application can be executed to handle authentication requests.
# Run the application
```
java -jar target/ta-ms-login-0.0.1-SNAPSHOT.jar
```

## Endpoints
1. Login Endpoint

This endpoint is responsible for generating a token to verify user credentials.
```
@RequestMapping(value = "/login", method = RequestMethod.POST)
public ResponseEntity<ResponseGeneralDTO<AuthenticationResponse>> login(@RequestBody AuthenticationRequest loginReq) {
    return authenticationService.authenticate(loginReq);
}
```
* Endpoint: /api/sso/login (http://localhost:8086/api/sso/auth/login)
* Method: POST
* Request Body:
```
{
  "username": "firstUser",
  "password": "P@ssw0rd"
}
```
* Response
```
{
    "code": "00",
    "description": "SUCCESS",
    "result": {
        "jwt": "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJmaXJzdF91c2VyIiwidXNlcm5hbWUiOiJmaXJzdF91c2VyIiwiZXhwIjoxNzAxNTMzNzI1fQ.X0-l8tNWWE2k0si7OH-elBmEEoifYb4WpeCQTilyfWo"
    }
}
```
## 2. Check Token Validity Endpoint

This endpoint verifies whether a user's token is still valid
```
@RequestMapping(value = "/check-token", method = RequestMethod.GET)
public UserPrincipalDTO checkTokenValidity(Principal principal) {
    return authenticationService.checkUserPrincipal(principal);
}
```
* Endpoint: /api/sso/check-token (http://localhost:8086/api/sso/auth/check-token)
* Method: GET
* Request Header:
```
Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJmaXJzdF91c2VyIiwidXNlcm5hbWUiOiJmaXJzdF91c2VyIiwiZXhwIjoxNzAxNTMzNzI1fQ.X0-l8tNWWE2k0si7OH-elBmEEoifYb4WpeCQTilyfWo
```
* Response
```
{
    "username": "first_user",
    "credentials": "P@ssw0rd"
}
```
