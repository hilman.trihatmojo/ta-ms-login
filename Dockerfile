FROM eclipse-temurin:17-jdk-alpine

#WORKDIR /app
#
COPY target/ta-ms-login-0.0.1-SNAPSHOT.jar /app/ta-ms-login.jar
#
#COPY deploy/dev.env /app/.env

COPY src /home/app/src
COPY pom.xml /home/app
COPY target/ta-ms-login-0.0.1-SNAPSHOT.jar /app/ta-ms-login.jar
#EXPOSE 8080

# Expose port
EXPOSE 8086

CMD ["java", "-jar", "/app/ta-ms-login.jar"]
ENTRYPOINT ["java","-jar","ta-ms-login.jar"]
