package com.hilman.tamslogin.controller;

import com.hilman.tamslogin.dto.ResponseGeneralDTO;
import com.hilman.tamslogin.dto.UserPrincipalDTO;
import com.hilman.tamslogin.model.AuthenticationRequest;
import com.hilman.tamslogin.model.AuthenticationResponse;
import com.hilman.tamslogin.services.AuthenticationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/auth")
public class AuthenticationController {
    private final AuthenticationService authenticationService;

    public AuthenticationController(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<ResponseGeneralDTO<AuthenticationResponse>> login(@RequestBody AuthenticationRequest loginReq) {
        return authenticationService.authenticate(loginReq);
    }

    @RequestMapping(value = "/check-token", method = RequestMethod.GET)
    public UserPrincipalDTO checkTokenValidity(Principal principal) {
       return authenticationService.checkUserPrincipal(principal);
    }
}
