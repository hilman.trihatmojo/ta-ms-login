package com.hilman.tamslogin.services;

import com.hilman.tamslogin.dto.ResponseGeneralDTO;
import com.hilman.tamslogin.dto.UserPrincipalDTO;
import com.hilman.tamslogin.model.AuthenticationRequest;
import com.hilman.tamslogin.model.AuthenticationResponse;
import org.springframework.http.ResponseEntity;

import java.security.Principal;

public interface AuthenticationService {
    ResponseEntity<ResponseGeneralDTO<AuthenticationResponse>> authenticate(AuthenticationRequest request);

    UserPrincipalDTO checkUserPrincipal(Principal principal);
}
