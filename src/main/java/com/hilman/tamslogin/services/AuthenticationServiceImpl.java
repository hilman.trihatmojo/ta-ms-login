package com.hilman.tamslogin.services;

import com.hilman.tamslogin.dto.ResponseGeneralDTO;
import com.hilman.tamslogin.dto.UserPrincipalDTO;
import com.hilman.tamslogin.enumeration.Status;
import com.hilman.tamslogin.model.AuthenticationRequest;
import com.hilman.tamslogin.model.AuthenticationResponse;
import com.hilman.tamslogin.model.User;
import com.hilman.tamslogin.security.jwt.JwtUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.security.Principal;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {
    private final AuthenticationManager authenticationManager;

    private final JwtUtil jwtUtil;

    private final UserService userService;

    public AuthenticationServiceImpl(AuthenticationManager authenticationManager, JwtUtil jwtUtil, UserService userService) {
        this.authenticationManager = authenticationManager;
        this.jwtUtil = jwtUtil;
        this.userService = userService;
    }

    @Override
    public ResponseEntity<ResponseGeneralDTO<AuthenticationResponse>> authenticate(AuthenticationRequest loginReq) {
        try {
            Authentication authentication =
                    authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                            loginReq.getUsername(),
                            loginReq.getPassword())
                    );
            String username = authentication.getName();
            User user = new User(username);
            String token = jwtUtil.createToken(user);
            AuthenticationResponse response = new AuthenticationResponse(token);

            ResponseGeneralDTO<AuthenticationResponse> responseDTO = new ResponseGeneralDTO<>(Status.SUCCESS.getStatusCode(), Status.SUCCESS.getDescription(), response, null);
            return new ResponseEntity<>(responseDTO, HttpStatus.OK);
        } catch (BadCredentialsException e) {
            ResponseGeneralDTO<AuthenticationResponse> responseDTO = new ResponseGeneralDTO<>(Status.ERROR.getStatusCode(), Status.ERROR.getDescription(), null, "Invalid username or password");
            return new ResponseEntity<>(responseDTO, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            ResponseGeneralDTO<AuthenticationResponse> responseDTO = new ResponseGeneralDTO<>(Status.ERROR.getStatusCode(), Status.ERROR.getDescription(), null, e.getMessage());
            return new ResponseEntity<>(responseDTO, HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public UserPrincipalDTO checkUserPrincipal(Principal principal) {
        if (principal == null) {
            throw new BadCredentialsException("Your session already expired. Please login again");
        }
        User user = userService.findUserByUserName(principal.getName());

        return new UserPrincipalDTO(user);
    }
}
