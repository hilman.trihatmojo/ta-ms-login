package com.hilman.tamslogin.services;

import com.hilman.tamslogin.model.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.Optional;

public interface UserService extends UserDetailsService {
    UserDetails loadUserByUsername(String username);

    User findUserByUserName(String username);
}
