package com.hilman.tamslogin.model;

import lombok.*;

@Data
@Builder
public class AuthenticationResponse {
    private final String jwt;

    public AuthenticationResponse(String jwt) {
        this.jwt = jwt;
    }
}
