package com.hilman.tamslogin.dto;

import com.hilman.tamslogin.model.User;
import lombok.Data;

@Data
public class UserPrincipalDTO {
    private String username;
    private String credentials;

    public UserPrincipalDTO(User user) {
        this.username = user.getUsername();
        this.credentials = user.getPassword();
    }
}
