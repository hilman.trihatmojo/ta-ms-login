package com.hilman.tamslogin.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class ResponseCheckTokenDTO {
    private String message;
    private String details;
}
