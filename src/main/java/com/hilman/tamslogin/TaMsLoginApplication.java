package com.hilman.tamslogin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaMsLoginApplication {

	public static void main(String[] args) {
		SpringApplication.run(TaMsLoginApplication.class, args);
	}

}
