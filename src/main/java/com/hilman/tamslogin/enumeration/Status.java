package com.hilman.tamslogin.enumeration;

import lombok.Getter;

@Getter
public enum Status {
    ERROR("06", "FAILED"),
    SUCCESS("00", "SUCCESS");

    private final String statusCode;
    private final String description;

    Status(String statusCode, String description) {
        this.statusCode = statusCode;
        this.description = description;
    }

}
