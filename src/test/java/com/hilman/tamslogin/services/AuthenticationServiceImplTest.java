package com.hilman.tamslogin.services;

import com.hilman.tamslogin.dto.ResponseGeneralDTO;
import com.hilman.tamslogin.dto.UserPrincipalDTO;
import com.hilman.tamslogin.enumeration.Status;
import com.hilman.tamslogin.model.AuthenticationRequest;
import com.hilman.tamslogin.model.AuthenticationResponse;
import com.hilman.tamslogin.model.User;
import com.hilman.tamslogin.security.jwt.JwtUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;

import java.security.Principal;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AuthenticationServiceImplTest {

    @Mock
    private AuthenticationManager authenticationManager;

    @Mock
    private JwtUtil jwtUtil;

    @Mock
    private UserService userService;

    @InjectMocks
    private AuthenticationServiceImpl authenticationService;

    @Test
    void authenticate_shouldReturnsSuccessResponse_WhenCalledWithValidCredentials() {
        AuthenticationRequest authenticationRequest = new AuthenticationRequest("username", "password");
        AuthenticationResponse authenticationResponse = new AuthenticationResponse("token");
        when(authenticationManager.authenticate(any())).thenReturn(mock(org.springframework.security.core.Authentication.class));
        when(jwtUtil.createToken(any())).thenReturn(authenticationResponse.getJwt());

        ResponseEntity<ResponseGeneralDTO<AuthenticationResponse>> responseEntity = authenticationService.authenticate(authenticationRequest);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(Status.SUCCESS.getStatusCode(), Objects.requireNonNull(responseEntity.getBody()).getCode());
        assertEquals(authenticationResponse.getJwt(), responseEntity.getBody().getResult().getJwt());
        verify(authenticationManager, times(1)).authenticate(any());
        verify(jwtUtil, times(1)).createToken(any());
    }

    @Test
    void authenticate_shouldReturnsErrorResponse_whenInvalidCredentials() {
        AuthenticationRequest authenticationRequest = new AuthenticationRequest("invalidUsername", "invalidPassword");
        when(authenticationManager.authenticate(any())).thenThrow(new BadCredentialsException("Invalid username or password"));

        ResponseEntity<ResponseGeneralDTO<AuthenticationResponse>> responseEntity = authenticationService.authenticate(authenticationRequest);

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertEquals(Status.ERROR.getStatusCode(), responseEntity.getBody().getCode());
        assertEquals("Invalid username or password", responseEntity.getBody().getError());
        verify(authenticationManager, times(1)).authenticate(any());
        verify(userService, never()).findUserByUserName(any());
        verify(jwtUtil, never()).createToken(any());
    }

    @Test
    void checkUserPrincipal_shouldReturnsUserPrincipalDTO_whenCalledWithValidPrincipal() {
        Principal principal = mock(Principal.class);
        User user = new User("testUser");
        UserPrincipalDTO userPrincipalDTO = new UserPrincipalDTO(user);
        when(userService.findUserByUserName(any())).thenReturn(user);

        UserPrincipalDTO result = authenticationService.checkUserPrincipal(principal);

        assertEquals(userPrincipalDTO, result);
        verify(userService, times(1)).findUserByUserName(any());
    }
}
