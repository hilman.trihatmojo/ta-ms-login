package com.hilman.tamslogin.controller;

import com.hilman.tamslogin.dto.ResponseGeneralDTO;
import com.hilman.tamslogin.dto.UserPrincipalDTO;
import com.hilman.tamslogin.enumeration.Status;
import com.hilman.tamslogin.model.AuthenticationRequest;
import com.hilman.tamslogin.model.AuthenticationResponse;
import com.hilman.tamslogin.model.User;
import com.hilman.tamslogin.services.AuthenticationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.security.Principal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class AuthenticationControllerTest {

    @Mock
    private AuthenticationService authenticationService;

    @InjectMocks
    private AuthenticationController authenticationController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void login_expectLoginToCallAuthenticationServiceAndReturnOkResponse_whenInvoked() {
        AuthenticationRequest authenticationRequest = new AuthenticationRequest("username", "password");
        AuthenticationResponse authenticationResponse = new AuthenticationResponse("token");
        ResponseGeneralDTO<AuthenticationResponse> responseDTO = new ResponseGeneralDTO<>(Status.SUCCESS.getStatusCode(), Status.SUCCESS.getDescription(), authenticationResponse, null);
        when(authenticationService.authenticate(authenticationRequest)).thenReturn(ResponseEntity.ok(responseDTO));

        ResponseEntity<ResponseGeneralDTO<AuthenticationResponse>> responseEntity = authenticationController.login(authenticationRequest);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(responseDTO, responseEntity.getBody());

        verify(authenticationService, times(1)).authenticate(authenticationRequest);
    }

    @Test
    void checkTokenValidity_expectToReturnUserPrincipalDTO_whenInvoked() {
        Principal principal = mock(Principal.class);
        User mockUser = User.builder()
                .username("us1")
                .password("Pass")
                .build();
        UserPrincipalDTO userPrincipalDTO = new UserPrincipalDTO(mockUser);
        when(authenticationService.checkUserPrincipal(principal)).thenReturn(userPrincipalDTO);

        UserPrincipalDTO result = authenticationController.checkTokenValidity(principal);

        assertEquals(userPrincipalDTO, result);
        verify(authenticationService, times(1)).checkUserPrincipal(principal);
    }
}
